<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    private $paginator;
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findUser(bool $byId = false, int $id = 1)
    {
        $qb = $this->createQueryBuilder('u')
        ->select('u','e','m','ea', 'g','bg')
        ->innerJoin('u.education', 'e')
        ->innerJoin('u.mobileNumber', 'm')
        ->innerJoin('u.emailAddress', 'ea')
        ->innerJoin('u.gender', 'g')
        ->innerJoin('u.bloodGroup', 'bg');

        if($byId) {
            $qb->where('u.id = :id')
                ->setParameter('id', $id); 
        }
        $query = $qb->getQuery();
        return $query->execute();
    }
}
