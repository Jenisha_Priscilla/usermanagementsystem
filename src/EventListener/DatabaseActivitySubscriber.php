<?php
namespace App\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class DatabaseActivitySubscriber implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::postUpdate,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $this->logActivity('persist', $args);
    }

    public function postUpdate(LifecycleEventArgs $args): void
    {
        $this->logActivity('update', $args);
    }
    
    private function logActivity(string $action, LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        if(!method_exists($entity, 'setUpdatedAt')) return;
        if($action == 'persist') $entity->setCreatedAt(new \DateTime());
        $entity->setUpdatedAt(new \DateTime());
    }
}
