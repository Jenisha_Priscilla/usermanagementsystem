<?php

namespace App\Entity;

class Education
{
    private $course;

    private $domain;

    private $university;

    private $obtainedPercentage;

    private $createdAt;

    private $updatedAt;

    private $id;

    private $user;

    public function getCourse(): ?string
    {
        return $this->course;
    }

    public function setCourse($course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain($domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getUniversity(): ?string
    {
        return $this->university;
    }

    public function setUniversity($university): self
    {
        $this->university = $university;

        return $this;
    }

    public function getObtainedPercentage(): ?float
    {
        return $this->obtainedPercentage;
    }

    public function setObtainedPercentage($obtainedPercentage): self
    {
        $this->obtainedPercentage = $obtainedPercentage;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
