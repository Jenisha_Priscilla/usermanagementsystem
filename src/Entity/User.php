<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class User
{
    private $firstName;

    private $lastName;

    private $dateOfBirth;

    private $createdAt;

    private $updatedAt;

    private $id;

    private $bloodGroup;

    private $gender;

    private $education;

    private $mobileNumber;

    private $emailAddress;

    public function __construct()
    {
        $this->education = new ArrayCollection();
        $this->mobileNumber = new ArrayCollection();
        $this->emailAddress = new ArrayCollection();
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName($firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName($lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth($dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBloodGroup(): ?BloodGroup
    {
        return $this->bloodGroup;
    }

    public function setBloodGroup(?BloodGroup $bloodGroup): self
    {
        $this->bloodGroup = $bloodGroup;

        return $this;
    }

    public function getGender(): ?Gender
    {
        return $this->gender;
    }

    public function setGender(?Gender $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return Collection|Education[]
     */
    public function getEducation(): Collection
    {
        return $this->education;
    }

    public function addEducation(Education $education): self
    {
        if (!$this->education->contains($education)) {
            $this->education[] = $education;
            $education->setUser($this);
        }

        return $this;
    }

    public function removeEducation(Education $education): self
    {
        if ($this->education->removeElement($education)) {
            // set the owning side to null (unless already changed)
            if ($education->getUser() === $this) {
                $education->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MobileNumber[]
     */
    public function getMobileNumber(): Collection
    {
        return $this->mobileNumber;
    }

    public function addMobileNumber(MobileNumber $mobileNumber): self
    {
        if (!$this->mobileNumber->contains($mobileNumber)) {
            $this->mobileNumber[] = $mobileNumber;
            $mobileNumber->setUser($this);
        }

        return $this;
    }

    public function removeMobileNumber(MobileNumber $mobileNumber): self
    {
        if ($this->mobileNumber->removeElement($mobileNumber)) {
            // set the owning side to null (unless already changed)
            if ($mobileNumber->getUser() === $this) {
                $mobileNumber->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmailAddress[]
     */
    public function getEmailAddress(): Collection
    {
        return $this->emailAddress;
    }

    public function addEmailAddress(EmailAddress $emailAddress): self
    {
        if (!$this->emailAddress->contains($emailAddress)) {
            $this->emailAddress[] = $emailAddress;
            $emailAddress->setUser($this);
        }

        return $this;
    }

    public function removeEmailAddress(EmailAddress $emailAddress): self
    {
        if ($this->emailAddress->removeElement($emailAddress)) {
            // set the owning side to null (unless already changed)
            if ($emailAddress->getUser() === $this) {
                $emailAddress->setUser(null);
            }
        }

        return $this;
    }
}
