<?php
namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\DataTransformer\GenderTransformer;
use App\Form\DataTransformer\BloodGroupTransformer;
use App\Form\Type\EducationType;
use App\Form\Type\MobileNumberType;
use App\Form\Type\EmailAddressType;
use App\Entity\User;
use App\Entity\Gender;
use App\Entity\BloodGroup;

class UserType extends AbstractType 
{
    private $genderTransformer;
    private $entityManager;
    private $bloodGroupTransformer;

    public function __construct(GenderTransformer $genderTransformer, BloodGroupTransformer $bloodGroupTransformer, EntityManagerInterface $entityManager)
    {
        $this->genderTransformer = $genderTransformer;
        $this->bloodGroupTransformer = $bloodGroupTransformer;
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class)
            ->add('dateOfBirth', DateType::class,
            array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ))
            ->add('gender', TextType::class, [
                'invalid_message' => 'That is not a gender',
            ])
            ->add('bloodGroup', TextType::class, [
                'invalid_message' => 'That is not a valid blood group',
            ])
            ->add('education', CollectionType::class, [
                'entry_type' => EducationType::class,
            ])
            ->add('mobileNumber', CollectionType::class, [
                'entry_type' => MobileNumberType::class,
            ])
            ->add('emailAddress', CollectionType::class, [
                'entry_type' => EmailAddressType::class, 
            ])
            ;

            $builder->get('gender')
            ->addModelTransformer($this->genderTransformer);

            $builder->get('bloodGroup')
            ->addModelTransformer($this->bloodGroupTransformer);

    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}