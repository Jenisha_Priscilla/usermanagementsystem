<?php

namespace App\Form\DataTransformer;

use App\Entity\BloodGroup;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class BloodGroupTransformer implements DataTransformerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function reverseTransform($bloodGroupAsString): ?bloodGroup
    {
        $bloodGroup = $this->entityManager
            ->getRepository(BloodGroup::class)
            ->findOneBy(['bloodGroup' => $bloodGroupAsString]);

        if (null === $bloodGroup) {
            throw new TransformationFailedException(sprintf(
                'Blood group as "%s" does not exist!',
                $bloodGroupAsString
            ));
        }
        return $bloodGroup;
    }

    public function transform($bloodGroup): string
    {
        if (null === $bloodGroup) {
            return '';
        }
        return $bloodGroup->getBloodGroup();
    }  
}