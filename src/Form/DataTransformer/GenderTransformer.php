<?php

namespace App\Form\DataTransformer;

use App\Entity\Gender;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class GenderTransformer implements DataTransformerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function reverseTransform($genderAsString): ?gender
    {
       $gender = $this->entityManager
                ->getRepository(Gender::class)
                ->findOneBy(['gender' => $genderAsString]);
                
        if (null === $gender) {
            throw new TransformationFailedException(sprintf(
                'Gender as "%s" does not exist!',
                $genderAsString
            ));
        }
        return $gender;
    }

    public function transform($gender): string
    {
        if (null === $gender) {
            return '';
        }
        return $gender->getGender();
    }  
}