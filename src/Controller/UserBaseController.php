<?php
namespace App\Controller;
use App\Entity\User;
use App\Controller\BaseController;
use App\Form\Type\UserType;
use Symfony\Component\HttpFoundation\Request;

class UserBaseController extends BaseController
{
    public function userForm(User $user, $dataToForm, $action) 
    {
        $form = $this->createForm(UserType::class, $user);
        $form->submit($dataToForm);
        $entity = $this->submitForm($form, $user, $action);
        return $entity;
    }

    public function serializeUser($user)
    {
        foreach($user->getEmailAddress() as $emailAddress) {
            $emailAddresses[] = ['emailAddress' => $emailAddress->getEmailAddress()];
        }
        foreach($user->getMobileNumber() as $mobileNumber) {
            $mobileNumbers[] = ['mobileNumber' => $mobileNumber->getMobileNumber()];
        }
        foreach($user->getEducation() as $education) {
           $educationDetails[] = 
           ['course' => $education->getCourse(),
            'university' => $education->getUniversity(),
            'domain' => $education->getDomain(),
            'obtainedPercentage'=> $education->getObtainedPercentage()];
        }
        return array(
            'id' => $user->getId(),
            'firstName' => $user->getFirstName(),
            'lastName' => $user->getLastName(),
            'dateOfBirth' => $user->getDateOfBirth(),
            'gender' => $user->getGender()->getGender(),
            'bloodGroup' => $user->getBloodGroup()->getBloodGroup(),
            'education' => $educationDetails,
            'mobileNumber' => $mobileNumbers,
            'emailAddress' => $emailAddresses
        );
    } 

    public function findUserById(int $id)
    {
        $user = $this->getDoctrine()
                ->getRepository(User::class)
                ->findUser(true, $id);

        return $user;
    }

    public function getMethod(Request $request)
    {
        $request->attributes->get('_controller');
        $params = explode('::',$request->attributes->get('_controller'));
        return $params[1];
    }
}