<?php
namespace App\Controller;
use App\Controller\UserBaseController;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Entity\Gender;
use App\Entity\BloodGroup;
use App\Entity\MobileNumber;
use App\Entity\Education;
use App\Entity\EmailAddress;
use App\Form\Type\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;

class UserController extends UserBaseController
{
    public const EDUCATION = 'education';
    public const MOBILE_NUMBER = 'mobileNumber';
    public const EMAIL_ADDRESS = 'emailAddress';
    public const INITIAL_VALUE = 0;
    public const NO_DATA = 'No data';
    public const NOT_FOUND = 'Not found';
    public const USERS_PER_PAGE = 5;

    public function add(Request $request)
    {
        $data = $request->getContent();
        $dataToForm = json_decode($data, true);
        $user = new User();
        $gender = new Gender();
        $bloodGroup = new BloodGroup();
        $user->setGender($gender);
        $user->setBloodGroup($bloodGroup);  
        $action = $this->getMethod($request);

        for($education = static::INITIAL_VALUE; $education < count($dataToForm[static::EDUCATION]); $education++)
        {
            $user->addEducation(new Education());
        }

        for($mobileNumber = static::INITIAL_VALUE; $mobileNumber < count($dataToForm[static::MOBILE_NUMBER]); $mobileNumber++)
        {
            $user->addMobileNumber(new MobileNumber());
        }

        for($emailAddress = static::INITIAL_VALUE; $emailAddress < count($dataToForm[static::EMAIL_ADDRESS]); $emailAddress++)
        {
            $user->addEmailAddress(new EmailAddress());
        }

        $result = $this->userForm($user, $dataToForm, $action);

        if ($result !== false) {
            return $this->addUpdateSuccessResponse($dataToForm, $action);
        }

        return $this->addUpdateFailureResponse($this->getError(), $action);
    }

    public function userById(int $id)
    {
        $user = $this->findUserById($id);
        if(count($user)) {
            $data['user']= $this->serializeUser($user[0]);
            return $this->readSuccessResponse($data);
        }
        return $this->readFailureResponse(static::NOT_FOUND);
    }

    public function fetchBloodGroup()
    {
        $bloodGroups = $this->getDoctrine()
                ->getRepository(BloodGroup::class)
                ->findAll();

        if($bloodGroups) {
            foreach ($bloodGroups as $bloodGroup) {
                $data['bloodGroup'][] = $bloodGroup->getBloodGroup();
            }
            return $this->readSuccessResponse($data);
        }
        return $this->readFailureResponse(static::NOT_FOUND);
    }

    public function fetchAllUser(PaginatorInterface $paginator, int $currentPage = 1)
    {
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findUser();

        $paginateUsers = $paginator->paginate($users, $currentPage , static::USERS_PER_PAGE);

        if(count($paginateUsers)) {
            foreach ($paginateUsers as $user) {
                $data['user'][] = $this->serializeUser($user);
            }
            $data['count'] = ceil(count($users)/static::USERS_PER_PAGE);
            return $this->readSuccessResponse($data);
        }
        return $this->readFailureResponse(static::NO_DATA);
    }

    public function update(Request $request, int $id)
    {
        $data = $request->getContent();
        $action = $this->getMethod($request);
        $dataToForm = json_decode($data, true);
        $user = $this->findUserById($id);
        
        if(!$user) {
            return $this->readFailureResponse(static::NOT_FOUND);   
        }
        $result = $this->userForm($user[0], $dataToForm, $action);
        if ($result !== false) {
            return $this->addUpdateSuccessResponse($dataToForm, $action);
        }
        return $this->addUpdateFailureResponse($this->getError(), $action);
    }
}


