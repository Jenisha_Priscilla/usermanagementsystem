<?php
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormInterface;

class BaseController extends AbstractController
{
    private $entityManager;
    private $error = [];
    public const HTTP_OK = 200;
    public const HTTP_NOT_FOUND = 404;
    public const HTTP_BAD_REQUEST = 400;
    public const ADD_SUCCESS = 'Data added successfully';
    public const ADD_FAIL = 'Failed to add data';
    public const UPDATE_SUCCESS = 'Data updated successfully';
    public const UPDATE_FAIL = 'Failed to update data';
    public const READ_SUCCESS = 'Data found';

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function submitForm(FormInterface $form, $entity, $action)
    {
        if($form->isSubmitted() && $form->isValid()) {
            return $this->DatabaseActivity($entity, $action);
        } 
        $this->setErrorFromForm($form);
        return false;
    }

    public function DatabaseActivity($entity, $action) 
    {
        if($action == 'add')  $this->entityManager->persist($entity);
        $this->entityManager->flush();
        return true;
    }

    public function getError(): array
    {
        return $this->error;
    }

    public function setErrorFromForm(FormInterface $form)
    {
        $validationError = $this->getErrorFromForm($form);
        array_walk($validationError, function ($value, $key) {
            $this->error[$key] = $value;
        });
    }

    private function getErrorFromForm(FormInterface $form)
    {
        $errors = [];

        foreach($form->getErrors() as $error) {
            $errors = $error->getMessage();
        }
        foreach($form->all() as $childForms) {
            if($childForms instanceof FormInterface) {
                if($childErrors = $this->getErrorFromForm($childForms)) {
                    $errors[$childForms->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }


    public function addUpdateSuccessResponse($data, $action)
    {
        if($action === 'add') $message = static::ADD_SUCCESS;
        else $message = static::UPDATE_SUCCESS;
        $result = [
            'message' => $message,
            'data' => $data
        ];
        return $this->sendResponse($result, static::HTTP_OK);
    }

    public function addUpdateFailureResponse($error, $action)
    {
        if($action === 'add') $message = static::ADD_FAIL;
        else $message = static::UPDATE_FAIL;
        
        $result = [
            'message' => $message,
            'error' => $error
        ];
        return $this->sendResponse($result, static::HTTP_BAD_REQUEST);
    }

    public function readSuccessResponse($data)
    {
        $result = [
            'message' => static::READ_SUCCESS,
            'data' => $data
        ];
        return $this->sendResponse($result, static::HTTP_OK);
    }

    public function readFailureResponse(string $message)
    {
        if($message === 'Not found') $code = static::HTTP_NOT_FOUND;
        $code = static::HTTP_OK;
        $result = [
            'message' => $message,
        ];
        return $this->sendResponse($result, $code);
    }

    public function sendResponse($result, $code) 
    {
        return new Response(json_encode($result), $code, [
            'Content-Type' => 'application/json'
        ]);
    }
}